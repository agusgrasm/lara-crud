<?php

if(!function_exists('to_rupiah'))
{
	function to_rupiah($number = 0){
		return number_format($number, 0, '.', ',');
	}
}