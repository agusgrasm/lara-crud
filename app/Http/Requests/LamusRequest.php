<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LamusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'namapm' => 'required',
            'image' => 'required|mimes:jpeg,bmp,png|max:2000',
            'alamat' => 'required',
            'jmlbantuan' => 'required',
            'jk' => 'required',
            'kusia' => 'required',
            'pdk' => 'required',
            'pkr' => 'required',
            'kasnaf' => 'required'

        ];

        if($this->isMethod('patch'))
            $rules['image']='mimes:jpeg,bmp,png|max:2000';
        return $rules;

    }

    public function messages()
    {
        return [
            'namapm.required' => 'Nama PM tidak boleh kosong',
            'image.required' => 'Image tidak boleh kosong',
            'alamat.required' => 'Alamat tidak boleh kosong',
            'jmlbantuan.required' => 'Jumlah Bantuan tidak boleh kosong',
            'jk.required' => 'Jenis Kelamin tidak boleh kosong',
            'kusia.required' => 'Kelompok Usia tidak boleh kosong',
            'pdk.required' => 'Pendidikan tidak boleh kosong',
            'pkr.required' => 'Pekerjaan tidak boleh kosong',
            'kasnaf.required' => 'Kategori Asnaf tidak boleh kosong'
        ];
    }
}
