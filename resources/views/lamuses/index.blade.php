@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('layouts.alert')
            <div class="panel panel-default">
                <div class="panel-heading">Lamus</div>
                <div class="panel-body">
                    {!!Form::open(['method' => 'get', 'target' => '_blank'])!!} 
                    {!!Form::hidden('search', request('search'))!!}
                    {!!Form::hidden('action', 'print')!!}
                    <button type="submit" class="btn btn-default pull-left"><i class="glyphicon glyphicon-print"></i> Print</button>
                    {!!Form::close()!!} 


                    {!!Form::open(['method' => 'get'])!!} 
                    <button type="submit" class="btn btn-default pull-right"><i class="glyphicon glyphicon-search"></i></button>
                            {!!Form::text('search', null, ['class' => 'form-control pull-right', 'style' => 'width: 350px;margin-right: 5px;'])!!}
                            <div class="pull-right" style="margin-right: 5px;padding-top: 5px;">Search</div>
                          
                    {!!Form::close()!!} 
                    <div style="clear: both;"></div>
                    <hr style="margin-bottom: 5px;margin-top: 15px;">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama PM</th>
                                    <th>Image</th>
                                    <th>Alamat</th>
                                    <th>Jumlah Bantuan</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Kelmpok Usia</th>
                                    <th>Pendidikan</th>
                                    <th>Pekerjaan</th>
                                    <th>Kategori Asnaf</th>

                                    <th>Author</th>
                                    <th>Last Updated</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($lamuses as $i=> $lamus)
                                {{-- @foreach($posts as $post) --}}
                                <tr>
                                    <td>{{$i+1}}</td>
                                    {{-- <td>{{$post->id}}</td> --}}
                                    <td>{{$lamus->namapm}}</td>
                                    <td>{!!$lamus->image!!}</td>
                                    <td>{{$lamus->alamat}}</td>
                                    {{-- <td class="text-right"> --}}
                                    <td>{{to_rupiah($lamus->jmlbantuan)}}</td>
                                    <td>{{$lamus->jk}}</td>
                                    <td>{{$lamus->kusia}}</td>
                                    <td>{{$lamus->pdk}}</td>
                                    <td>{{$lamus->pkr}}</td>
                                    <td>{{$lamus->kasnaf}}</td>

                                    <td>{{$lamus->user_name}}</td>
                                    <td>{{$lamus->updated_at->format('d/m/Y H:i')}}</td>
                                    <td>
                                         {!! link_to(route('lamuses.edit', ['id' => $lamus->id]), 'edit', ['class' => 'btn btn-xs btn-primary']) !!} 
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>    
                    {!! $lamuses->appends(request()->all())->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<style type="text/css">
    .table thead tr th{
        vertical-align: middle;
        white-space: nowrap;
    }
</style>
@endpush
