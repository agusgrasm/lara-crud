<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('users')->middleware('auth')->group(function () {
    Route::get('index', 'UserController@index')->name('users.index');
    Route::get('create', 'UserController@create')->name('users.create');
    Route::put('store', 'UserController@store')->name('users.store');
    Route::get('{id}/edit', 'UserController@edit')->name('users.edit');
    Route::patch('{id}/update', 'UserController@update')->name('users.update');
    Route::delete('{id}/delete', 'UserController@destroy')->name('users.delete');
});

Route::prefix('posts')->middleware('auth')->group(function () {
    Route::get('index', 'PostController@index')->name('posts.index');
    Route::get('create', 'PostController@create')->name('posts.create');
    Route::put('store', 'PostController@store')->name('posts.store');
    Route::get('{id}/edit', 'PostController@edit')->name('posts.edit');
    Route::patch('{id}/update', 'PostController@update')->name('posts.update');
    Route::delete('{id}/delete', 'PostController@destroy')->name('posts.delete');
   
});

Route::prefix('lamuses')->middleware('auth')->group(function () {
    Route::get('index', 'LamusController@index')->name('lamuses.index');
    Route::get('create', 'LamusController@create')->name('lamuses.create');
    Route::put('store', 'LamusController@store')->name('lamuses.store');  
    Route::get('{id}/edit', 'LamusController@edit')->name('lamuses.edit');
    Route::patch('{id}/update', 'LamusController@update')->name('lamuses.update');
    Route::delete('{id}/delete', 'LamusController@destroy')->name('lamuses.delete');
});