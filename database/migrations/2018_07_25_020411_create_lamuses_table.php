<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLamusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lamuses', function (Blueprint $table) {
            $table->increments('id');

            $table->string('namapm', 255);
            $table->string('image', 255);
            $table->string('alamat', 255);
            $table->integer('jmlbantuan');
            $table->enum('jk', ['L','P']);
            $table->enum('kusia', ['Balita','Anak - Anak','Remaja','Dewasa','Manula']);
            $table->enum('pdk', ['Tidak Sekolah','TK / PAUD',' SD','SMP','SMA','Diploma','Sarjana']);
            $table->enum('pkr', ['Kary. Swasta','Petani','Pedagang','Guru','Supir','Pelajar / Mahasiswa','IRT','Buruh','Tidak Bekerja']);
            $table->enum('kasnaf', ['Fakir','Miskin','Gharimin','Mualaf','Fisabi lillah','Ibnu Sabil','Hamba Sahaya']);

            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')->on('users')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lamuses');
    }
}
