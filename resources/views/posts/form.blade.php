@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('layouts.alert')
                <div class="panel panel-default">
                    <div class="panel-heading">Form Post</div>
                    {!! Form::model($model, [
                        'url' => route('posts.' . ($model->id ? 'update' : 'store'), $model->id ? ['id' => $model->id] : []),
                        'method' => $model->id ? 'patch' : 'put',
                        'id' => 'form'
                    ]) !!}
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{$errors->has('title') ? ' has-error' : ''}}">
                                    {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
                                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                    @if($errors->has('title'))
                                    <p class="help-block">{{implode(',', $errors->get('title', []))}}</p>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group{{$errors->has('jenis') ? ' has-error' : ''}}">
                                    {!! Form::label('jenis', 'Jenis', ['class' => 'control-label']) !!}
                                    {!! Form::select('jenis',['' => '--pilih--','jenis-1' => 'jenis-1','jenis-2','jenis-3'], null, ['class' => 'form-control']) !!}
                                    @if($errors->has('jenis'))
                                    <p class="help-block">{{implode(',', $errors->get('jenis', []))}}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group{{$errors->has('content') ? ' has-error' : ''}}">
                                    {!! Form::label('content', 'Content', ['class' => 'control-label']) !!}
                                    {!! Form::textArea('content', null, ['class' => 'form-control', 'rows'=> '3']) !!}
                                    @if($errors->has('content'))
                                    <p class="help-block">{{implode(',', $errors->get('content', []))}}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{$errors->has('category') ? ' has-error' : ''}}">
                                    {!! Form::label('category', 'Category', ['class' => 'control-label']) !!}
                                    {!! Form::text('category', null, ['class' => 'form-control']) !!}
                                    @if($errors->has('category'))
                                    <p class="help-block">{{implode(',', $errors->get('category', []))}}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="panel-footer">
                        {!!Form::open(['url' => route('posts.delete', ['id'=>$model->id]), 'method' => 'delete', 'class'=>'pull-left'])!!}
                    {!!Form::submit('Hapus',['class'=>'btn btn-danger','onclick' => 'if(!confirm("Are you sure want to delete data?"))return false'])!!}
                    {!!Form::close()!!}
                        {!! Form::button('Save', ['class' => 'btn btn-primary pull-right', 'onClick' => '$("#form").submit()']) !!}
                        <div style="clear: both;"></div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
@endsection
