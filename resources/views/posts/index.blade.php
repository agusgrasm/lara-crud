@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('layouts.alert')
            <div class="panel panel-default">
                <div class="panel-heading">Post</div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>Jenis</th>
                                    <th>Category</th>
                                    <th>Author</th>
                                    <th>Last Updated</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($posts as $i=> $post)
                                {{-- @foreach($posts as $post) --}}
                                <tr>
                                    <td>{{$i+1}}</td>
                                    {{-- <td>{{$post->id}}</td> --}}
                                    <td>{{$post->title}}</td>
                                    <td>{{$post->content}}</td>
                                    <td>{{$post->jenis}}</td>
                                    <td>{{$post->category}}</td>
                                    <td>{{$post->user->name}}</td>
                                    <td>{{$post->updated_at->format('d/m/Y H:i')}}</td>
                                    <td>
                                        {!! link_to(route('posts.edit', ['id' => $post->id]), 'edit', ['class' => 'btn btn-xs btn-primary']) !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $posts->appends(request()->all())->links() !!}
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
