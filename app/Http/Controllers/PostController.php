<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Post;

class PostController extends Controller
{
    public function index(Request $request)
    {
        $posts = Post::paginate($request->get('length', 10));
        // dd($posts);
        return view('posts.index', get_defined_vars());
    }

    public function create()
    {
        $model = new Post();
        return view('posts.form', get_defined_vars());
    }

    public function store(PostRequest $request)
    {
         // dd($request->all());
        \DB::beginTransaction();
        try{
            $request->offSetSet('user_id', \Auth::id());
            $preData = $request->only(['title', 'content', 'jenis', 'category', 'user_id']);
            // dd($preData);
            $preData['remember_token'] = str_random(10);
            if($model = Post::create($preData)){
                //dd($model);
                \DB::commit();
            return redirect ("posts/index"); 

            }
        } catch(\Exception $e){
            \DB::rollBack();
            dd($e);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $model = Post::findOrFail($id);
        return view('posts.form', get_defined_vars());
    }

    public function update(PostRequest $request, $id)
    {
        $model = Post::findOrFail($id);
        // dd($model, $request->all());
        \DB::beginTransaction();
        try{
            if($model->update($request->all())){
                // dd($model);
                \DB::commit();
                \Session::flash('success', 'Data Sudah Diupdate');
                return redirect("posts/index");
                // return redirect("posts/$model->id/edit");
            }
        } catch(\Exception $e){
            \DB::rollBack();
            \Session::flash('fail', 'Tidak Dapat Mengubah Data');
            return back();
        }
    }

    public function destroy($id)
    {
        $model = Post::findOrFail($id);
        // dd($model, $request->all());
        \DB::beginTransaction();
        try{
            if($model->delete()){
                // dd($model);
                \DB::commit();
                \Session::flash('success', 'Data Sudah Dihapus');
                return redirect("posts/index");
            }
        } catch(\Exception $e){
            \Session::flash('fail', 'Tidak Dapat Menghapus Data');
            \DB::rollBack();
            dd($e);
        }
    }
}
