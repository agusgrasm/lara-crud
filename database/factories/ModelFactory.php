<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];

});

$factory->define(App\Post::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->name,
        'content' => $faker->text,
        'category' => $faker->randomElement(['category-1', 'category-2', 'category-3']),
        'jenis' => $faker->randomElement(['jenis-1', 'jenis-2', 'jenis-3']),
        'user_id' => \DB::select('SELECT id FROM users ORDER BY RAND() LIMIT 1')[0]->id
    ];

});

$factory->define(App\Lamus::class, function (Faker\Generator $faker) {
    return [
        'namapm' => $faker->name,
        'image' => $faker->name,
        'alamat' => $faker->name,
        'jmlbantuan' => $faker->numberBetween(500000, 5000000),

        
        'jk' => $faker->randomElement(['L','P']),
        'kusia' => $faker->randomElement(['Balita','Anak - Anak','Remaja','Dewasa','Manula']),
        'pdk' => $faker->randomElement(['Tidak Sekolah','TK / PAUD',' SD','SMP','SMA','Diploma','Sarjana']),
        'pkr' => $faker->randomElement(['Kary. Swasta','Petani','Pedagang','Guru','Supir','Pelajar / Mahasiswa','IRT','Buruh','Tidak Bekerja']),
        'kasnaf' => $faker->randomElement(['Fakir','Miskin','Gharimin','Mualaf','Fisabi lillah','Ibnu Sabil','Hamba Sahaya']),

        'user_id' => \DB::select('SELECT id FROM users ORDER BY RAND() LIMIT 1')[0]->id
    ];

});