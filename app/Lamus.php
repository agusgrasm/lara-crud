<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lamus extends Model
{
    protected $fillable = ['namapm', 'image', 'alamat', 'jmlbantuan', 'jk', 'kusia', 'pdk', 'pkr', 'kasnaf', 'user_id'];

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function getImageAttribute($value)
    {
    	if(file_exists(public_path("photo/$value")) && is_file(public_path("photo/$value")))
        	$image = '<img src="' . asset("photo/$value") . '" class="img-rounded" width="100px">';
        else
        	$image = '<img src="' . asset("img/avatar.png") . '" class="img-rounded" width="100px">';
    	return $image;
    }
}
