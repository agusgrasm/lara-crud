<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LamusRequest;
use App\Lamus;

class LamusController extends Controller
{
    public function index(Request $request)
    {
        $model = Lamus::select([
            'lamuses.*',
            'users.name AS user_name'
        ])
            ->leftJoin('users', 'users.id','=','lamuses.user_id');
        if($param = $request->search){
            $model = $model->orWhere('namapm', 'like', "%$param%");
            $model = $model->orWhere('alamat', 'like', "%$param%");
            $model = $model->orWhere('jmlbantuan', 'like', "%$param%");
            $model = $model->orWhere('jk', 'like', "%$param%");
            $model = $model->orWhere('kusia', 'like', "%$param%");
            $model = $model->orWhere('pdk', 'like', "%$param%");
            $model = $model->orWhere('pkr', 'like', "%$param%");
            $model = $model->orWhere('kasnaf', 'like', "%$param%");
            $model = $model->orWhere('users.name', 'like', "%$param%");
            // dd($model->toSql());
        }

        if($request->action == 'print')
            return $this->_print($model->get());

        $lamuses = $model->paginate($request->get('length', 10));
        // dd($posts);
        return view('lamuses.index', get_defined_vars());
    }

    public function create()
    {
        $model = new Lamus();
        return view('lamuses.form', get_defined_vars());
    }

    public function store(LamusRequest $request)
    {
         // dd($request->all());
        \DB::beginTransaction();
        try{
            $request->offSetSet('user_id', \Auth::id());
            $preData = $request->only(['namapm', 'image', 'alamat', 'jmlbantuan', 'jk', 'kusia', 'pdk', 'pkr', 'kasnaf', 'user_id']);
            // dd($preData);
            $preData['remember_token'] = str_random(10);
            if($model = Lamus::create($preData)){
                //dd($model);
                \DB::commit();
            return redirect ("lamuses/index"); 

            }
        } catch(\Exception $e){
            \DB::rollBack();
            dd($e);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $model = Lamus::findOrFail($id);
        return view('lamuses.form', get_defined_vars());
    }

    public function update(LamusRequest $request, $id)
    {
         $model = Lamus::findOrFail($id);
        // dd($model, $request->all());
        \DB::beginTransaction();
        try{
            $data = $request->all();
            if ($request->hasFile('image') && $image = $request->image->store('', 'photo')) {
                $data['image'] = $image;
            }
            if($model->update($data)){
                \DB::commit();
                \Session::flash('success', 'Data Sudah Diupdate');
                return redirect("lamuses/index");
                // return redirect("posts/$model->id/edit");
            }
        } catch(\Exception $e){
            dd($e);
            \DB::rollBack();
            \Session::flash('fail', 'Tidak Dapat Mengubah Data');
            return back();
        }
    }

    public function destroy($id)
    {
        $model = Lamus::findOrFail($id);
        // dd($model, $request->all());
        \DB::beginTransaction();
        try{
            if($model->delete()){
                // dd($model);
                \DB::commit();
                \Session::flash('success', 'Data Sudah Dihapus');
                return redirect("lamuses/index");
            }
        } catch(\Exception $e){
            \Session::flash('fail', 'Tidak Dapat Menghapus Data');
            \DB::rollBack();
            dd($e);
        }
    }

    protected function _print($model)
    {
        // return view('lamuses.print', get_defined_vars());
        $pdf = \PDF::setOptions([
            'dpi' => 150, 
            'defaultFont' => 'sans-serif'
        ])
            ->setPaper('a4', 'landscape')
            ->loadView('lamuses.print', get_defined_vars());
        return $pdf->stream('invoice.pdf');
    }
}
