@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Form User</div>
                    {!! Form::model($model, [
                        'url' => route('users.' . ($model->id ? 'update' : 'store'), $model->id ? ['id' => $model->id] : []),
                        'method' => $model->id ? 'patch' : 'put'
                    ]) !!}
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
                                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        {!! link_to_route('users.delete','Delete', ['id'=>$model->id], ['class' => 'btn btn-danger', 'onclick' => 'if(!confirm("Are you sure want to delete data?"))return false']) !!}
                        {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
