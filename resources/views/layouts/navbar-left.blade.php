{{--<li><a href="{{ route('users.index') }}">Data User</a></li>--}}
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Data User <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{ route('users.index') }}">List</a></li>
        <li><a href="{{ route('users.create') }}">Create</a></li>
    </ul>
</li>

{{--<li><a href="{{ route('posts.index') }}">Post</a></li>--}}
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Post <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{ route('posts.index') }}">List</a></li>
        <li><a href="{{ route('posts.create') }}">Create</a></li>
    </ul>
</li>

{{--<li><a href="{{ route('posts.index') }}">Master Data</a></li>--}}
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Master Data <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{ route('lamuses.index') }}">List</a></li>
        <li><a href="{{ route('lamuses.create') }}">Create</a></li>
    </ul>
</li>

{{--<li><a href="{{ route('posts.index') }}">Transaksi</a></li>--}}
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Transaksi <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="#">List</a></li>
        <li><a href="#">Create</a></li>
    </ul>
</li>