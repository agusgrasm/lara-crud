<?php

use Illuminate\Database\Seeder;

class LamusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Lamus::class, 100)->create();
    }
}
