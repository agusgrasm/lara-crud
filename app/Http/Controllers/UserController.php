<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index(Request $request)
    {
    	$users = User::paginate($request->get('length', 10));
    	return view('users.index', get_defined_vars());
    }

    public function create()
    {
        $model = new User();
        return view('users.form', get_defined_vars());
    }

    public function store(Request $request)
    {
        // dd($request->all());
        \DB::beginTransaction();
        try{
            $preData = $request->only(['name', 'email']);
            $preData['password'] = bcrypt('secret');
            $preData['remember_token'] = str_random(10);
            if($model = User::create($preData)){
                //dd($model);
                \DB::commit();
            return redirect ("users/$model->id/edit"); 

            }
        } catch(\Exception $e){
            \DB::rollBack();
            dd($e);
        }
    }

    public function edit($id)
    {
        $model = User::findOrFail($id);
        return view('users.form', get_defined_vars());
    }

    public function update(Request $request, $id)
    {
        $model = User::findOrFail($id);
        // dd($model, $request->all());
        \DB::beginTransaction();
        try{
            if($model->update($request->all())){
                // dd($model);
                \DB::commit();
                return redirect("users/$model->id/edit");
            }
        } catch(\Exception $e){
            \DB::rollBack();
            dd($e);
        }
    }

    public function destroy(Request $request, $id)
    {
        $model = User::findOrFail($id);
        // dd($model, $request->all());
        \DB::beginTransaction();
        try{
            if($model->delete()){
                // dd($model);
                \DB::commit();
                return redirect("users/index");
            }
        } catch(\Exception $e){
            \DB::rollBack();
            dd($e);
        }
    }
}
